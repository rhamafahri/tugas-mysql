1. Membuat DATABASE
CREATE DATABASE myshop;


2. Membuat Tabel
- Membuat tabel users
CREATE TABLE users( id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), email VARCHAR(255), password VARCHAR(255) ); 

- Membuat tabel categories
CREATE TABLE categories ( id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255) ); 

- Membuat tabel items + foreign key
CREATE TABLE items ( id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), description VARCHAR(255), price INT, stock INT, category_id INT NOT NULL, FOREIGN KEY(category_id) REFERENCES categories(id) ); 


3. Memasukan Data pada Table
- Memasukan data pada table users
INSERT INTO users(name, email, password) VALUES ("John Doe", "john@doe.com", "john123"); 
INSERT INTO users(name, email, password) VALUES ("Jane Doe", "jane@doe.com", "jenita123"); 

- Memasukan data pada tabel categories
INSERT INTO categories(name) VALUES ("gadget");
INSERT INTO categories(name) VALUES ("cloth");
INSERT INTO categories(name) VALUES ("men");
INSERT INTO categories(name) VALUES ("women");
INSERT INTO categories(name) VALUES ("branded");

- Memasukan data pada tabel items
INSERT INTO items(name, description, price, stock, category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1);
INSERT INTO items(name, description, price, stock, category_id) VALUES ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2);
INSERT INTO items(name, description, price, stock, category_id) VALUES ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

4 Mengambil data users
a. SELECT id, name, email FROM users;
b. - SELECT price FROM items WHERE price > 1000000;
   - SELECT name FROM items WHERE name LIKE "uniklo";
c. SELECT items.name, items.description, items.price, items.stock, categories.name FROM items JOIN categories ON items.category_id = categories.id;

5. Mengubah Data dari Database
UPDATE items SET price=2500000 WHERE id=1;